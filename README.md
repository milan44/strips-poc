This is a proof of concept, do not use this if the strips system instance is not yours!

I am not reliable for any damaged caused by this software. Use at your own risk!

**Usage:**
- open strips-poc.exe
- Enter url to strips system (without path! example: https://strips.system instead of https://strips.system/index.php)
- Log into the strips system
- Open up the dev tools in your browser (F12)
- Go to cookies
- Copy the cookie named PHPSESSID
- When asked for "Enter PHP session ID" paste that in
- Done

**You have to be logged into the strips system for the poc to work**

The fact that this took me only about 15 minutes to build should be concerning.